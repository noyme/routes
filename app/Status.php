<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name'];

    public function candidates(){
        return $this->hasMany('App\Candidate');
    }

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        return self::find($nextstages)->all();
    }

    public static function allowed($to,$from){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to', $to)->get();
        if(isset($allowed))
            return TRUE;
        return FALSE;
    }

}