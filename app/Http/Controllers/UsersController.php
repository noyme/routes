<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();
        $userroles = Userrole::all();
        return view('users.index', compact('users','departments','userroles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.show', compact('user','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('add-user');
        $user = User::findOrFail($id);
        $departments =Department::all();
        return view('users.edit', compact('user','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('add-user');
        $user = user::findOrFail($id);
       if(!isset($request->password)){
        $request['password'] = $user->password;   
       }else{
         $request->password = Hash::make($request['password']);   
       } 
       $user->update($request->all()); 
        return redirect('users.index');  
    }

    public function changedepartment(Request $request){
        $uid = $request->id;
        $did = $request->department_id;
        $user = User::findOrFail($uid);
        if($user->candidates->isEmpty()){
            $user->department_id = $did;
            $user->save();
            return back();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the departments of the user becuase he is owner of candidates');
        }
        return back();       
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users.index'); 
    }
}
