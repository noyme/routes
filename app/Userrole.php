<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userrole extends Model
{
    protected $fillable = [
        'user_is', 'role_id',
    ];
}
