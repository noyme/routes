<?php


use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('statuses')->insert(
        [
            'name' =>  'Before Interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('statuses')->insert(
        [
            'name' => 'Not Fit',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

         DB::table('statuses')->insert(
        [
            'name' => 'Sent to Manager',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        DB::table('statuses')->insert(
        [
            'name' => 'Not fit Professionally',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

         DB::table('statuses')->insert(
        [
            'name' =>  'Accepted To Work',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

    }

}

