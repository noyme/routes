<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;



class InterviewsSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date'=>'2020-07-15',
                'Summary' => 'not good',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date'=>'2020-07-15',
                'Summary' => 'very good',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]                            
            ]);
    }
}
