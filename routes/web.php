<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web rou
tes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')-> middleware('auth');

Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete');

Route::get('candidates/changeUser/[{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('/user.candidates','CandidatesController@myCandidates')->name('user.candidates')->middleware('auth');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');

Route::get('user/delete/{id}','UsersController@destroy')->name('users.delete')->middleware('auth');
Route::get('users.index','UsersController@index')->name('users.index')->middleware('auth');
Route::get('user/edit/{id}','UsersController@edit')->name('users.edit')->middleware('auth');
Route::post('user/update/{id}','UsersController@update')->name('users.update')->middleware('auth');
Route::get('user.details/{id}','UsersController@show')->name('users.show')->middleware('auth');
Route::post('user.changedepartment','UsersController@changedepartment')->name('users.changedepartment')->middleware('auth');

Route::get('users.changerole/{uid}', 'UserrolesController@create')->name('users.makemanager')->middleware('auth');
Route::get('role/delete/{id}','UserrolesController@destroy')->name('role.delete')->middleware('auth');


Route::get('/interviews','InterviewsController@index')->name('interviews.index')->middleware('auth');
Route::get('interviews/create','InterviewsController@create')->name('interviews.create')->middleware('auth');
Route::post('/interviews','InterviewsController@store')->name('interviews.store')->middleware('auth');
Route::get('/user.interviews','InterviewsController@myInterviews')->name('user.interviews')->middleware('auth');





Route::get('/hello',function(){
    return'Hello Larevel';
});

Route::get('/student/{id}',function($id = 'No student'){
    return'We got student with id '.$id;
});

Route::get('/car/{id?}',function($id = null){
    if (isset($id)){
        //TODO: validate for integer
        return "We got car $id ";
    } else {
        return 'We need the id to find your car';
    }
});

//Route::get('/users/{email}/{name?}', function ($email = null , $name = 'Name missing') {
//    return view('users', compact('name' , 'email'));
//});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
