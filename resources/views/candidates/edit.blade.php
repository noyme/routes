@extends('layouts.app')
@section('title','Edit candidate')
@section('content')
<h1>Edit Candidates</h1>
<form method = "post" action = "{{action('CandidatesController@update',$candidate->id )}}">
    @method('PATCH')
    @csrf
    <div class="input-group mb-3">
        <label for = "name" class="input-group-text" id="inputGroup-sizing-default">Candidate name</label>
        <input type = "text" name = "name" value = {{$candidate->name}} class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
    </div>
    <div class="input-group mb-3">
        <label for = "email" class="input-group-text" id="inputGroup-sizing-default">Candidate email</label>
        <input type = "text" name = "email" value = {{$candidate->email}} class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
    </div>
    <div>
        <input type = "submit" name = "submit" value = "Update candidate" class="btn btn-secondary">
    </div>
    </form>          
@endsection