@extends('layouts.app')
@section('title','Create candidate')
@section('content')
<h1>Create Candidates</h1>
<form method = "post" action = "{{action('CandidatesController@store')}}">
@csrf
<div class="input-group mb-3">
    <label for = "name" class="input-group-text" id="inputGroup-sizing-default">Candidate name</label>
    <input type = "text" name = "name" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div class="input-group mb-3">
    <label for = "email" class="input-group-text" id="inputGroup-sizing-default">Candidate email</label>
    <input type = "text" name = "email" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div>
    <input type = "submit" name = "submit" value = "Create candidate" class="btn btn-secondary">
</div>

</form>           
@endsection
