@extends('layouts.app')
@section('title','Create interview')
@section('content')
<h1>Create interview</h1>
<form method = "post" action = "{{route('interviews.store')}}">
@csrf
<div class="input-group mb-3">
    <label for = "date" class="input-group-text" id="inputGroup-sizing-default">Interview date</label>
    <input type = "date" name = "date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div class="form-group row">
    <label for="candidate_id" class="col-md-4 col-form-label text-md-right">{{ __('Candidate') }}</label>
    <div class="col-md-6" data-toggle="dropdown">
        <select class="form-control" name="candidate_id">                                                                     
            @foreach ($candidates as $candidate)
              <option value= {{ $candidate->id }}> 
                  {{ $candidate->name }} 
              </option>
            @endforeach    
          </select>                      
    </div>
</div>
<div class="form-group row">
    <label for="user_id" class="col-md-4 col-form-label text-md-right">{{ __('Interviewer') }}</label>
    <div class="col-md-6" data-toggle="dropdown">
        <select class="form-control" name="user_id">                                                                         
            @foreach ($users as $user)
            @if($user_id == $user->id)
                <option value="{{ $user->id}}" selected="selected"> 
                    {{ $user->name }} 
                </option>
            @else
                <option value="{{ $user->id}}"> 
                    {{ $user->name }} 
                </option>
            @endif
            @endforeach    
          </select>                      
    </div>
</div>
<div class="input-group mb-3">
    <label for = "summary" class="input-group-text" id="inputGroup-sizing-default">Interview summary</label>
    <input type = "text" name = "summary" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div>
    <input type = "submit" name = "submit" value = "Create interview" class="btn btn-secondary">
</div>

</form>           
@endsection
