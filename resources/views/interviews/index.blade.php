@extends('layouts.app')
@section('title','List of Interviews')
@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div class="btn btn-outline-info"><a href = "{{route('interviews.create')}}">Add new interview</a></div>
    <h1>List of interviews</h1>
    <table class="table table-hover">
        <tr>
            <th>ID</th><th>Date</th><th>Summary</th><th>candidate</th><th>Interviewer</th>
        </tr>
        <!-- the table data-->
        @foreach($interviews as $interview)   
            <tr>
                <td>{{$interview->id}}</td>
                <td>{{$interview->date}}</td>
                <td>{{$interview->summary}}</td>
                <td>{{$interview->candidate->name}}</td>
                <td>{{$interview->user->name}}</td>
        @endforeach
    </table>
@endsection