@extends('layouts.app')
@section('title','User Details')
@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
    <h1>User Details</h1>
    <table class="table table-hover">
        <tr>
            <th>ID</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th>
        </tr>
        <!-- the table data-->
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>

            </tr>
    </table>
    <form method="POST" action="{{ route('users.changedepartment') }}">
        @csrf  
        <div class="form-group row">
        <label for="user" class="col-md-4 col-form-label text-md-right">Move to status</label>
        <div class="col-md-6">
            <select class="form-control" name="department_id">                                                                         
              @foreach ($departments as $department)
              @if($user->department_id == $department->id)
              <option value="{{ $department->id }}" selected = "selected"> 
                  {{ $department->name }} 
              </option>
              @else
              <option value="{{ $department->id }}"> 
                {{ $department->name }} 
                </option>
                @endif
              @endforeach    
            </select>
        </div>
        <input name="id" type="hidden" value = {{$user->id}} >
        <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Change department
                    </button>
                </div>
        </div>                    
    </form> 
    </div>
@endsection