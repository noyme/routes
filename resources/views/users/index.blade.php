@extends('layouts.app')
@section('title','List of Users')
@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
    <h1>List of Users</h1>
    <table class="table table-hover">
        <tr>
            <th>ID</th><th>Name</th><th>Email</th><th>Department</th><th>Role</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th><th>Details</th>
        </tr>
        <!-- the table data-->
        @foreach($users as $user) 
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    {{$user->department->name}}
                </td>  
                <td>@foreach ($user->roles as $role)
                    <option value="{{ $role->id }}"> 
                        {{ $role->name }} 
                    </option>
                    @endforeach
                </td>    
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td>
                    <a href = "{{route('users.edit',$user->id)}}">Edit</a>
                </td>
                <td>
                    <a href = "{{route('users.delete',$user->id)}}">Delete</a>
                </td>
                <td>
                    <a href = "{{route('users.show',$user->id)}}">Details</a>
                </td>
                @foreach($userroles as $userrole)
            @if($userrole->user_id == $user->id &&(!($userrole->role_id == '2' )))
                <td>
                    <a href = "{{route('users.makemanager',$user->id)}}">Make manager</a>

                </td>

                @elseif($userrole->user_id == $user->id &&( $userrole->role_id == '2' || $userrole->role_id == ''))
                <td>
                    <a href = "{{route('role.delete',$user->id)}}">Cancel manager role</a>
                </td>
                @endif
                @endforeach
            </tr>
        @endforeach
    </table>
@endsection